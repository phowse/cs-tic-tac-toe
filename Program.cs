﻿using System;

namespace cstictactoe
{
    class Program
    {
        // Initialise a random number
        static Random rnd = new Random();

        // Create a grid of squares
        static char[] grid = new char[] {'-','-','-','-','-','-','-','-','-'};
        static char turn = 'x'; // player x starts

        static int numberMoves = 0; // stops us going more than 9 moves
        static bool gameon = true;  // stops the game if someone wins

        // Simple if statement to check the 8 ways you can win. Yes there are 
        // only 8 ways to win. This section can be improved.
        static bool CheckWin()
        {
            if(grid[0] == grid[1] && grid[1] == grid[2] && grid[0] != '-')
            {
                return true;
            }
            else if(grid[3] == grid[4] && grid[4] == grid[5] && grid[3] != '-')
            {
                return true;
            }
            else if(grid[6] == grid[7] && grid[7] == grid[8] && grid[6] != '-')
            {
                return true;
            }
            else if(grid[0] == grid[3] && grid[3] == grid[6] && grid[0] != '-')
            {
                return true;
            }
            else if(grid[1] == grid[4] && grid[4] == grid[7] && grid[1] != '-')
            {
                return true;
            }
            else if(grid[2] == grid[5] && grid[5] == grid[8] && grid[2] != '-')
            {
                return true;
            }
            else if(grid[0] == grid[4] && grid[4] == grid[8] && grid[0] != '-')
            {
                return true;
            }
            else if(grid[2] == grid[4] && grid[4] == grid[6] && grid[2] != '-')
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // Function to print the formatted grid. Another way would be to use
        // Console.Write() which would allow us to change the color, etc.
        static string PrintGrid()
        {
            string output = "\n\n[" + 
                grid[0] + "][" + 
                grid[1] + "][" +
                grid[2] + "]\n[" + 
                grid[3] + "][" +
                grid[4] + "][" + 
                grid[5] + "]\n[" +
                grid[6] + "][" + 
                grid[7] + "][" +
                grid[8] + "]" + 
                "\n\n";
            return output;
        }

        // A different way to print the grid where we write each character to the
        // console allowing us to change colors
        static void AltPrintGrid()
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Red;

            Console.Write("\n\n");      // set the space at the top
            int cols = 3;               // number of columns for the game
            int col = 1;                // current count of the colums

            // loop through each square in the grid
            foreach(char sq in grid)
            {
                // change the color based on whose turn it is
                if(sq == 'x'){
                    Console.ForegroundColor = ConsoleColor.Green;
                }
                else if(sq == 'o')
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                }

                // Print the square. The {0} allows us to print a variable in
                // a string
                Console.Write("[{0}]", sq);

                // Here we use the modulus of of the col/cols division. The mod
                // returns the remainder of a division. So, if the remainder is
                // a 0, we know we have gone three times through the loop and 
                // can add an end line character.
                if(col % cols == 0)
                {
                    Console.Write("\n");
                }

                // Set defaults and variables
                col++;
                Console.ForegroundColor = ConsoleColor.Red;
            }
            Console.Write("\n\n");
        }

        static void Main(string[] args)
        {
            // Set initial colors. Cos' why not?
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Red;
            
            // Console.Clear() clears the content of the terminal
            Console.Clear();

            // Calls the function which returns a string representation of the grid
            //Console.Write(PrintGrid()); 
            AltPrintGrid();

            // while we have more moves and noone has won yet
            while(numberMoves < 9 && gameon == true)
            {
                // Get input from the user
                Console.Write("Enter your move (1-9): ");
                string moveSquare = Console.ReadLine();

                // declare an int to store the move
                int square = 0;

                // Error check wrong input (not an integer)
                if(!int.TryParse(moveSquare, out square))
                {
                    Console.WriteLine("Improper input. Try again plaver " + turn);
                }

                // Check for valid move
                else if (square >= 1 && square <=9 && grid[square-1] == '-')
                {
                    grid[square - 1] = turn;
                    
                    Console.Clear();
                    //Console.Write(PrintGrid()); 
                    AltPrintGrid();

                    // make sure noone has won yet
                    if(CheckWin() == true)
                    {
                        // Inform player that they have won and end the game
                        Console.WriteLine(turn + " has won!\n\n");
                        gameon = false;
                    }
                    else
                    {
                        // VERY straightforward random number computer player
                        bool placed = false;   // have we placed the 'o' yet?
                        while(placed == false) // while we haven't placed it yet
                        {
                            int move = rnd.Next(0,8);   // get a random number
                            if(grid[move] == '-')       // if random number is free
                            {
                                grid[move] = 'o';       // go on random space
                                placed = true;          // say that we have placed it
                                Console.Clear();
                                //Console.Write(PrintGrid()); 
                                AltPrintGrid();
                            }
                        }
                        numberMoves++;  // make sure we don't go too far
                    }
                }
                else
                {
                    Console.WriteLine("Improper input. Try again plaver " + turn);
                }
            }
        }
    }
}